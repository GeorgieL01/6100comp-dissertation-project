-- Create a schema "dissertationDB" in order to insert data into the web application. 

drop table if exists CLUBS;
drop table if exists PLAYERS;
drop table if exists PREVIOUSsearches;
drop table if exists USERS;

CREATE Table USERS(
Username char, 
PWord char (20),
PRIMARY KEY (Username)
);

CREATE Table PREVIOUSsearches(
SearchID char,
Username  char, 
SearchQuery varchar (30),
PRIMARY KEY (searchID),
FOREIGN KEY (Username) REFERENCES USERS(Username)
);

CREATE Table PLAYERS(
playerID int,
playerName varchar (40),
clubName varchar (30),
playerAge int (2),
playerNationality varchar (40),
playerPosition varchar (20),
gamesPlayed int,
minutesPlayed varchar(10),
gamesStarted int,
tacklesMade int, 
interceptionsMade int,
pressuresMade int,
blocksMade int,
goalsScored int,
assistsMade int,
shotsTaken int,
passCompletion float,
yellowCards int,
redCards int,
foulsGivenAway int,
offsidesGivenAway int,
PRIMARY KEY (playerID)
);

CREATE Table CLUBS(
clubID int auto_increment, 
clubName varchar (30),
matchesPlayed int, 
averageAge float,
averagePossesion float,
playersUsed int,
teamTackles int,
foulsGivenAway int,
offsidesGivenAway int,
teamInterceptions int,
teamGoals int,
teamAssists int,
teamShots int,
teamShotsOnTarget int,
teamRedCards int,
teamYellowCards int,
teamPressures int,
teamBlocks int, 
PRIMARY KEY (clubID) 
);

SELECT * FROM PLAYERS;
SELECT * FROM CLUBS;